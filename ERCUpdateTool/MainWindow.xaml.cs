﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Management;
using System.Threading;
using System.ComponentModel;

using ERCConsole;

namespace ERCUpdateTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ERCStatus ercStatus = new ERCStatus { Status = "ERC Update Tool" };
        TextBasedConsole console = new TextBasedConsole();
        string selectedPort = string.Empty;
        enum updateState { waiting, gettingData, waitForProgramming, writingData }; 
        updateState currentState = updateState.waiting;
        ercData ercManData;

        struct ercData
        {
            public string pcbaserialnumber;
            public string pcbapartno;
            public string hlaserialnumber;
            public string hlapartno;
            public string mac;
            public string model;
            public string version;
        }
        public MainWindow()
        {
            List<string> mybutton = new List<string>();
            string full = string.Empty;

            InitializeComponent();
            this.DataContext = this;
            ercStatus.Status = "starting\n";
            // get the comm ports on the computer
            try
            {
                ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_PnPEntity");

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    if (queryObj["Caption"] != null)
                    {
                        if (queryObj["Caption"].ToString().Contains("(COM"))
                        {
                            full = queryObj["Caption"].ToString();
                            mybutton.Add(full.Substring(full.IndexOf("COM")).TrimEnd(')'));
                        }
                    }

                }
            }
            catch (ManagementException exx)
            {
                MessageBox.Show(exx.Message);
            }
            cmbERCPort.ItemsSource = mybutton;

        }

        private void btLookForSwitch_Click(object sender, RoutedEventArgs e)
        {
            ClearWindow();
            UpdateWindow("Started update at " + DateTime.Now.ToLocalTime() + "\n");
//            btLookForSwitch.IsEnabled = false;
            btLookForSwitch.Content = "Looking";
            var thread = new Thread(getERCData);
            thread.Start();
            //lbpcbsn.Content = ercManData.pcbaserialnumber;
            //lbpcbpn.Content = ercManData.pcbapartno;
            //lbhlasn.Content = ercManData.hlaserialnumber;
            //lbhlapn.Content = ercManData.hlapartno;
            //lbMAC.Content = ercManData.mac;
            //lbmodel.Content = ercManData.model;
            //btLookForSwitch.IsEnabled = true;
            //btLookForSwitch.Content = "Start looking for ERC";
        }

        private  void getERCData()
        {
            string outmsg = string.Empty;
            bool gotData = false;
            bool ercready = false;
            bool inconsole = false;             // set to true when in linux prompt
            bool inupdate = false;              // set to true when in update mode
            char[] trimchars = { '\n', '\r' };

            console.SetDumpFileName("c:\\reports\\", "gwtest");
            console.Open(selectedPort);
            console.ClearBuffers();

            ercManData.pcbapartno = "---";
            ercManData.pcbaserialnumber = "---";
            ercManData.hlapartno = "---";
            ercManData.hlaserialnumber = "---";
            ercManData.mac = "---";
            ercManData.model = "---";
            Dispatcher.Invoke((Action)(() => UpdateWindow("testing.,...")));

            if (console.LastErrorCode != 0)
            {
                doUpdateWindow("ERC port can not be opened. Error: " + console.LastErrorMessage + "\n");
            }
            else
            {
                doUpdateWindow("Looking for 'Switch> '\n");
                outmsg = console.GetCopyOfSerialBuffer();
                if (console.WaitForPrompt(10000, "Switch> ", out outmsg))
                {
                    console.WriteLine("s p r", 1000);            // put into run mode
                    if (console.WaitForPrompt(10000, "Switch> ", out outmsg))
                    {
                        Thread.Sleep(200);                      // make sure it processes the command
                        doUpdateWindow("ERC in run mode. Getting manufacture data\n");
                        console.WriteLine("d man", 1000);
                        if (console.WaitForPrompt(10000, "Switch> ", out outmsg))
                        {
                            //doUpdateWindow(outmsg);
                            // parse the data
                            ParseManData(outmsg, ref ercManData);
                            gotData = true;
                            // get version
                            console.WriteLine("d ver", 1000);
                            console.WaitForPrompt(1000, "Switch> ", out outmsg);
                            doUpdateWindow("code version = " + outmsg.TrimEnd(trimchars));
                        }
                        else
                        {
                            doUpdateWindow("Problem getting manufacture data.\n");
                        }
                    }
                    else
                    {
                        doUpdateWindow("Did not find prompt after setting to run mode.\n");
                    }
                }
                else
                {
                    doUpdateWindow("'Switch' not found. Try again.\n");
                }


            }
            console.Close();
            Dispatcher.Invoke((Action)(() => tbpcbsn.Text = ercManData.pcbaserialnumber));
            Dispatcher.Invoke((Action)(() => tbpcbpn.Text = ercManData.pcbapartno));
            Dispatcher.Invoke((Action)(() => tbhlasn.Text = ercManData.hlaserialnumber));
            Dispatcher.Invoke((Action)(() => tbhlapn.Text = ercManData.hlapartno));
            Dispatcher.Invoke((Action)(() => tbMAC.Text = ercManData.mac));
            Dispatcher.Invoke((Action)(() => tbmodel.Text = ercManData.model));
            if (ercManData.pcbapartno.Contains("--"))
            {
                //Dispatcher.Invoke((Action)(() => btLookForSwitch.IsEnabled = true));
                Dispatcher.Invoke((Action)(() => btLookForSwitch.Content = "Look for ERC Data"));
                doUpdateWindow("Did not find ERC data\n");
            }
            else
            {
                Dispatcher.Invoke((Action)(() => btLookForSwitch.Content = "Data Read"));
                //Dispatcher.Invoke((Action)(() => btLookForSwitch.IsEnabled = false));
                //Dispatcher.Invoke((Action)(() => btProgramERC.IsEnabled = true));
                doUpdateWindow("Data Read\n");
            }
        }

        private void btProgramERC_Click(object sender, RoutedEventArgs e)
        {
            btProgramERC.Content = "Looking";
            var thread = new Thread(do_btProgramERC_Click);
            thread.Start();
        }

        private void do_btProgramERC_Click()
        { 
            string outmsg = string.Empty;
            ercData fromWindow = new ercData();
            ercData readback = new ercData();
            char[] trimchars = { '\n', '\r' };


            Dispatcher.Invoke((Action)(() => fromWindow.mac = tbMAC.Text));
            Dispatcher.Invoke((Action)(() => fromWindow.model = tbmodel.Text));
            Dispatcher.Invoke((Action)(() => fromWindow.pcbaserialnumber = tbpcbsn.Text));
            Dispatcher.Invoke((Action)(() => fromWindow.pcbapartno = tbpcbpn.Text));
            Dispatcher.Invoke((Action)(() => fromWindow.hlaserialnumber = tbhlasn.Text));
            Dispatcher.Invoke((Action)(() => fromWindow.hlapartno = tbhlapn.Text));

            console.SetDumpFileName("c:\\reports\\", "gwtest");
            console.Open(selectedPort);
            console.ClearBuffers();
            Dispatcher.Invoke((Action)(() => btProgramERC.Content = "Programming Started"));
            outmsg = console.GetCopyOfSerialBuffer();
            if (console.WaitForPrompt(10000, "Switch> ", out outmsg))
            {
                doUpdateWindow("Found ERC. Programming now.\n");
                console.WriteLine("s p r", 1000);            // put into run mode
                if (console.WaitForPrompt(10000, "Switch> ", out outmsg))
                {
                    Thread.Sleep(200);                      // make sure it processes the command
                    doUpdateWindow("ERC in run mode. Programing manufacture data\n");
                    console.WriteLine(MakeManCmd(fromWindow), 1000);
                    if (console.WaitForPrompt(10000, "Switch> ", out outmsg))
                    {
                        // verify data
                        console.WriteLine("d man", 1000);
                        console.WaitForPrompt(1000, "Switch> ", out outmsg);
                        //doUpdateWindow(outmsg);
                        // parse the data
                        ParseManData(outmsg, ref readback);
                        if (!fromWindow.Equals(readback))
                        {
                            doUpdateWindow("\nData readback is BAD\n");
                        }
                        else
                            doUpdateWindow("\nData readback is GOOD.\n");

                        console.WriteLine("d ver", 1000);
                        console.WaitForPrompt(1000, "Switch> ", out outmsg);
                        doUpdateWindow("code version = " + outmsg.TrimEnd(trimchars));
                    }
                    else
                    {
                        doUpdateWindow("\nProblem getting manufacture data.\n");
                    }
                }
                else
                {
                    doUpdateWindow("\nDid not find prompt after setting to run mode.\n");
                }
            }
            else
            {
                doUpdateWindow("\n'Switch' not found. Try again.\n");
            }

            Dispatcher.Invoke((Action)(() => btProgramERC.Content = "Program ERC"));

            console.Close();

        }

    private void cmbERCPort_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

            selectedPort = cmbERCPort.SelectedItem.ToString();
            btLookForSwitch.IsEnabled = true;
            btProgramERC.IsEnabled = true;
        }

        private void doUpdateWindow(string text)
        {
            Dispatcher.Invoke((Action)(() => UpdateWindow(text)));
        }

        private void UpdateWindow(string status)
        {
            tbStatus.Text += status;
            tbStatus.ScrollToEnd();
        }

        private void ClearWindow()
        {
            tbStatus.Text = string.Empty;
        }

        private void ParseManData(string rawdata, ref ercData data)
        {
            char[] charsToTrim = { '\r', '\n' };
            ercManData.mac = "---";
            ercManData.pcbapartno = "---"; ;
            ercManData.pcbaserialnumber = "---";;
            ercManData.hlapartno = "---";;
            ercManData.hlaserialnumber = "---";;
            ercManData.model = "---";;

            string[] lines = rawdata.Split('\n');
            foreach (var item in lines)
            {
                if (item.Contains("MAC"))
                {
                    data.mac = item.Substring(item.IndexOf("=") + 2).TrimEnd(charsToTrim);
                }
                if (item.Contains("pcba_partno"))
                {
                    data.pcbapartno = item.Substring(item.IndexOf("=") + 2).TrimEnd(charsToTrim);
                }
                if (item.Contains("pcba_serialno"))
                {
                    data.pcbaserialnumber = item.Substring(item.IndexOf("=") + 2).TrimEnd(charsToTrim);
                }
                if (item.Contains("hla_partno"))
                {
                    data.hlapartno = item.Substring(item.IndexOf("=") + 2).TrimEnd(charsToTrim);
                }
                if (item.Contains("hla_serialno"))
                {
                    data.hlaserialnumber = item.Substring(item.IndexOf("=") + 2).TrimEnd(charsToTrim);
                }
                if (item.Contains("model"))
                {
                    data.model = item.Substring(item.IndexOf("=") + 2).TrimEnd(charsToTrim);
                }
            }
        }

        private string MakeManCmd(ercData data)
        {
            return "set man " + data.mac + " " +
                                        data.pcbapartno + " " +
                                        data.pcbaserialnumber + " " +
                                        data.hlapartno + " " +
                                        data.hlaserialnumber + " " +
                                        data.model;

            
        }
    }
    public class ERCStatus
    {

        private string status;
        public string Status
        {
            get { return status; }
            set
            {
                status = value;
            }
        }

    }
}
